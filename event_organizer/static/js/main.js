function submitInvolvement() {
    const button = document.activeElement;
    const mode = parseInt(button.getAttribute('mode'));
    const event_id = parseInt(button.getAttribute('event'));
    
    document.getElementById('id_mode').value = mode;
    document.getElementById('id_event_id').value = event_id;
    document.getElementById('eventInvolvementForm').submit();
}
