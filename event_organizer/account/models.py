from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Account(models.Model):
    """
    This is account model. This model is and extended model of django User model.
    """
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    phone = models.CharField(max_length=20,blank=True,null=True)
    profpic = models.ImageField(blank=True,null=True,upload_to='profpic')

    def get_events(self):
        """
        Get event involved for current account.
        Return
        ---------
        Queryset object
        """
        from event.models import EventInvolved
        records = EventInvolved.objects.select_related('event').filter(participant=self)
        return records