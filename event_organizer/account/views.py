from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView,UpdateView,DeleteView
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

from .models import Account
from .forms import AccountForm

# Create your views here.
def user_is_staff(user):
    if user.is_staff:
        return True
    else:
        return False

@login_required
@user_passes_test(user_is_staff)
def query_account(request):
    data = Account.objects.select_related('user').all()
    return render(request,'account_query.html',{'data':data})

@login_required
def detail_account(request,pk):
    if not request.user.is_staff and request.user.pk!=pk:
        return redirect('home')
    data = Account.objects.get(pk=pk)
    return render(request,'account_detail.html',{'data':data})

class CreateAccount(CreateView):
    form_class = AccountForm
    success_url = reverse_lazy('account')
    template_name = 'account_form.html'


class UpdateAccount(LoginRequiredMixin,UserPassesTestMixin,UpdateView):
    model = Account
    form_class = AccountForm
    context_object_name = 'data'
    template_name = 'account_form.html'

    def get_success_url(self):
        return reverse('detail_account',kwargs={'pk':self.object.pk})

    def test_func(self):
        data = self.get_object()
        if not user_is_staff(self.request.user) and data.user.pk != self.request.pk:
            return False
        else:
            return True


class DeleteAccount(LoginRequiredMixin,UserPassesTestMixin,DeleteView):
    model = Account
    template_name = 'account_delete.html'
    success_url = reverse_lazy('account')
    context_object_name = 'data'
    
    def test_func(self):
        data = self.get_object()
        if not user_is_staff(self.request.user) and data.user.pk != self.request.pk:
            return False
        else:
            return True