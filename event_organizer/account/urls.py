from django.urls import path
from . import views

urlpatterns = [
    path('',views.query_account,name='account'),
    path('create',views.CreateAccount.as_view(),name='create_account'),
    path('<int:pk>',views.detail_account,name='detail_account'),
    path('<int:pk>/update',views.UpdateAccount.as_view(),name='update_account'),
    path('<int:pk>/delete',views.DeleteAccount.as_view(),name='delete_account'),
]