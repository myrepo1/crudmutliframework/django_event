from django import forms
from django.contrib.auth.models import User
from re import match

from .models import Account

class AccountForm(forms.ModelForm):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150,required=False)
    password1 = forms.CharField(max_length=150,required=False,label='Password Confirmation')
    name = forms.CharField(max_length=150,required=False)

    class Meta:
        model = Account
        exclude = ['user'] 

    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        if self.instance.pk:
            self.fields['username'].initial = self.instance.user.username
            self.fields['name'].initial = self.instance.user.first_name

    def is_changed(self,field,val):
        """
        Check wether a field is changed from current record. This function used in modelform.
        Parameters
        ---------
        field: str
            Name of field to be checked
        val: any
            Value of field to be checked
        """
        if self.instance.pk:
            prev_val = getattr(self.instance,field)
            return prev_val != val
        else:
            return True

    def clean_username(self):
        data = self.cleaned_data.get('username')
        user = User.objects.filter(username=username).first()
        if (self.instance.pk and self.instance.user.username != data and user) or (not self.instance.pk and user):
            self.add_error('username','Username with that name already exist.')
        return data

    def clean_phone(self):
        data = self.cleaned_data.get('phone')
        if self.is_changed('phone',data):
            if data and not data.is_numeric():
                self.add_error('',f'{data} is not valid phone number. Only number input is allowed.')
        return data

    def clean(self):
        if any(self.errors):
            return
        password = self.cleaned_data.get('password')
        password1 = self.cleaned_data.get('password1')
        if password and not password1 or not password and password1:
            self.add_error('','Both password field must be filled.')
        elif password and password1 and password!=password1:
            self.add_error('','Password not match.')

    def save(self):
        if self.instance.pk:
            password = self.cleaned_data.get('password')
            username = self.cleaned_data.get('username')
            first_name = self.cleaned_data.get('name')
            user = self.instance.user
            user.username = username
            user.first_name = first_name
            if password:
                user.set_password(password)
            user.save()
            return super().save()
        else:
            account = super().save(commit=False)
            username = self.cleaned_data.get('username')
            password = self.cleaned_data.get('password')
            first_name = self.cleaned_data.get('name')
            user = User.objects.create_user(username,password=password,first_name=first_name)
            account.user = user
            account.save()
            return account