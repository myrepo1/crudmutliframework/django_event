from datetime import datetime, timedelta
from django.utils import timezone

now = timezone.now()

events = [
    {
        'name':'Bazaar Rame Tahunan',
        'shortname':'bazaar20',
        'start':now+timedelta(days=5),
        'end':now+timedelta(days=8),
        'description':'Event bazaar tahunan. Banyak jualan disini.',
        'location':'Gedung Sate'
        },
    {
        'name':'Theater Perjuangan Kemerdekaan 1945',
        'shortname':'TP45',
        'start':now+timedelta(days=4),
        'end':now+timedelta(days=4,hours=2),
        'description':'Pertunjukan proses perjuangan kemerdekaan.',
        'location':'Gedung Merdeka'
        },
    {
        'name':'Lomba lari marathon se Bandung',
        'shortname':'maraBDG',
        'start':now+timedelta(days=-5),
        'end':now+timedelta(days=-5,hours=5),
        'description':'Lomba lari marathon se bandung.',
        'location':'Jalan Merdeka'
    },
    {
        'name':'Layar Tancap Bandung Tempo Doeloe',
        'shortname':'oldBDG',
        'start':now+timedelta(days=3),
        'end':now+timedelta(days=3,hours=3),
        'description':'Nonton bareng gambaran Bandung tempo dulu.',
        'location':'Museum Sri Baduga'
    },
    {
        'name':'1000 Pohon hijaukan Negeri',
        'shortname':'1000hijau',
        'start':now+timedelta(days=10),
        'description':'Menanam pohon bersama untuk penghijauan.',
        'location':'Babakan Siliwangi'
    }
]

accounts = [
    {
        'user':{
            'username':'admin',
            'is_staff':True,
            'password':'admin321_123',
            'first_name':'Event Admin',
            },
        'account':{
            'phone':'08098912312'
        }
    },
    {
        'user':{
            'username':'person1',
            'password':'person_321',
            'first_name':'Budi',
        },
        'account':{
            'phone':'12930123213'
        }
    },
]

def create_account(data):
    from django.contrib.auth.models import User
    from account.models import Account
    user = User.objects.create_user(**data['user'])
    account = Account.objects.create(user=user,**data['account'])
    return account

def create_accounts(data):
    from django.contrib.auth.models import User
    from account.models import Account

    result = []
    for item in data:
        user = User.objects.create_user(**item['user'])
        account = Account.objects.create(user=user,**item['account'])
        result.append(account)
    return result

def create_event(data):
    from event.models import Event

    event = Event.objects.create(**data)
    return event

def create_events(data):
    from event.models import Event

    result = []
    for item in data:
        result.append(Event.objects.create(**item))
    return result

def to_str_event(data):
    if 'start' in data and data['start']:
        data['start'] = data['start'].strftime('%d-%m-%Y %H:%M')
    if 'end' in data and data['end']:
        data['end'] = data['end'].strftime('%d-%m-%Y %H:%M')

def copy_dict(data):
    result = {k:v for k,v in data.items()}
    return result