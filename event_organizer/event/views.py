from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView,UpdateView,DeleteView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.utils import timezone

from .models import Event, EventInvolved
from .forms import EventForm, InvolvementForm

# Create your views here.
def event(request):
    """
    View for home and event url. If user is authenticated, 
    """

    # if method is post, begin update involvement of current user in an event.
    if request.method == 'POST':
        #Redirect user to current_path if not logged in
        if not request.user.is_authenticated:
            return redirect(request.get_full_path())
        form = InvolvementForm(request.POST)
        if form.is_valid():
            mode = form.cleaned_data.get('mode')
            event_id = form.cleaned_data.get('event_id')
            event = Event.objects.get(pk=event_id)
            account = request.user.account
            if mode == 0:
                EventInvolved.objects.create(event=event,participant=account)
            elif mode == 1:
                event_involved = EventInvolved.objects.get(event=event,participant=account)
                event_involved.delete()
            return redirect(request.get_full_path())
    form = InvolvementForm()
    today = timezone.now().date()
    account_id = None
    if request.user.is_authenticated:
        account_id = request.user.account.id
        if request.path == '/':
            record_list = Event.objects.raw('SELECT e.id,name,shortname,start,end,ei.participant_id as account_exist FROM event_event as e LEFT JOIN event_eventinvolved as ei ON e.id == ei.event_id WHERE start>=date(%s) and (ei.participant_id==(%s) or ei.participant_id is null)',[today.strftime('%Y-%m-%d'),account_id])
        else:
            record_list = Event.objects.raw('SELECT e.id,name,shortname,start,end,ei.participant_id as account_exist FROM event_event as e LEFT JOIN event_eventinvolved as ei ON e.id == ei.event_id WHERE (ei.participant_id==(%s) or ei.participant_id is null)',[account_id])
    else:
        if request.path == '/':
            record_list = Event.objects.filter(start__date__gte=today)
        else:
            record_list = Event.objects.all()
    return render(request,'event.html',{'data':record_list,'account_id':account_id,'form':form})

def detail_event(request,pk):
    record = Event.objects.get(pk=pk)
    participants = record.get_participants()
    return render(request,'event_detail.html',{'data':record,'participants':participants})


class CreateEvent(LoginRequiredMixin,UserPassesTestMixin,CreateView):
    form_class = EventForm
    success_url = reverse_lazy('home')
    template_name = 'event_form.html'

    def test_func(self):
        if self.request.user.is_staff:
            return True
        else:
            return False


class UpdateEvent(LoginRequiredMixin,UserPassesTestMixin,UpdateView):
    model = Event
    form_class = EventForm
    template_name = 'event_form.html'
    context_object_name = 'data'
    def get_success_url(self):
        return reverse('detail_event',kwargs={'pk':self.object.pk})

    def test_func(self):
        if self.request.user.is_staff:
            return True
        else:
            return False


class DeleteEvent(LoginRequiredMixin,UserPassesTestMixin,DeleteView):
    model = Event
    template_name = 'event_delete.html'
    success_url = reverse_lazy('home')
    context_object_name = 'data'

    def test_func(self):
        if self.request.user.is_staff:
            return True
        else:
            return False