from django.test import TestCase
from django.urls import resolve,reverse

from custom_code.dummy_data import events,accounts,create_account,create_event

from ..views import event

class TestEventWithoutUser(TestCase):
    def setUp(self):
        self.record = create_event(events[0])
        self.url = reverse('home')
        self.response = self.client.get(self.url)

    def test_status_code(self):
        self.assertEqual(self.response.status_code,200)

    def test_url_show_correct_func(self):
        view = resolve('/')
        self.assertEqual(view.func.__name__,event.__name__)

    def test_view_show_correct_template(self):
        self.assertTemplateUsed(self.response,'event.html')

    def test_page_contain_correct_data(self):
        self.assertContains(self.response,self.record.shortname)
        self.assertContains(self.response,'Login')
        self.assertNotContains(self.response,'Logout')

class TestHomeWithUser(TestCase):
    def setUp(self):
        self.record = create_event(events[1])
        self.account = create_account(accounts[1])
        self.client.login(username=accounts[1]['user']['username'],password=accounts[1]['user']['password'])
        self.url = reverse('home')
        self.response = self.client.get(self.url)

    def test_page_contain_correct_data(self):
        self.assertContains(self.response,'Logout')
        self.assertContains(self.response,'My Account')
        self.assertNotContains(self.response,'Login')
        self.assertNotContains(self.response,'Accounts')


class TestHomeWithStaffUser(TestCase):
    def setUp(self):
        self.record = create_event(events[1])
        self.account = create_account(accounts[0])
        self.client.login(username=accounts[0]['user']['username'],password=accounts[0]['user']['password'])
        self.url = reverse('home')
        self.response = self.client.get(self.url)

    def test_page_contain_correct_data(self):
        self.assertContains(self.response,'Accounts')