from django.test import TestCase
from django.urls import reverse,resolve

from custom_code.dummy_data import events,accounts,create_account, copy_dict, to_str_event
from ..views import CreateEvent
from ..forms import EventForm
from ..models import Event

class TestCreateEventWithoutUser(TestCase):
    def setUp(self):
        self.url = reverse('create_event')
        self.response = self.client.get(self.url)

    def test_redirection(self):
        login_url = reverse('login')
        self.assertRedirects(self.response,f'{login_url}?next={self.url}')


class TestCreateEventWithUserNonStaff(TestCase):
    def setUp(self):
        account_data = copy_dict(accounts[1])
        self.account = create_account(account_data)
        self.client.login(username=account_data['user']['username'],password=account_data['user']['password'])
        self.url = reverse('create_event')
        self.response = self.client.get(self.url)

    def test_status_code(self):
        self.assertEqual(self.response.status_code,403)


class TestCreateEventWithUserStaff(TestCase):
    def setUp(self):
        self.account_data = copy_dict(accounts[0])
        self.account = create_account(self.account_data)
        self.client.login(username=self.account_data['user']['username'],password=self.account_data['user']['password'])
        self.url = reverse('create_event')
        self.response = self.client.get(self.url)

    def test_status_code(self):
        self.assertEqual(self.response.status_code,200)

    def test_url_show_correct_func(self):
        view = resolve('/event/create')
        self.assertEqual(view.func.__name__,CreateEvent.__name__)

    def test_view_show_correct_template(self):
        self.assertTemplateUsed(self.response,'event_form.html')

    def test_view_show_correct_form(self):
        form = self.response.context.get('form')
        self.assertIsInstance(form,EventForm)

    def test_form_user_csrf(self):
        self.assertContains(self.response,'csrfmiddlewaretoken')

    def test_valid_input(self):
        input_data = copy_dict(events[0])
        to_str_event(input_data)
        self.client.post(self.url,input_data)
        self.assertTrue(Event.objects.exists())
        result = Event.objects.get(shortname=input_data['shortname'])
        self.assertEqual(result.name,input_data['name'])

    def test_valid_input_redirection(self):
        input_data = copy_dict(events[0])
        to_str_event(input_data)
        response = self.client.post(self.url,input_data)
        redirect_url = reverse('home')
        self.assertRedirects(response,redirect_url)

    def test_invalid_input_empty_field(self):
        input_data = {'name':'','start':''}
        response = self.client.post(self.url,input_data)
        self.assertFormError(response,'form','name','This field is required.')
        self.assertFormError(response,'form','start','This field is required.')

    def test_invalid_input_end_before_start(self):
        input_data = copy_dict(events[0])
        input_data['start'],input_data['end'] = input_data['end'],input_data['start']
        to_str_event(input_data)
        response = self.client.post(self.url,input_data)
        self.assertFormError(response,'form','','Start must be before end.')
