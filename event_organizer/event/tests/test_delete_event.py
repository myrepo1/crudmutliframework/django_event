from django.test import TestCase
from django.urls import reverse,resolve

from custom_code.dummy_data import events,accounts,create_account,create_event, copy_dict, to_str_event
from ..views import DeleteEvent
from ..models import Event

class TestDeleteEventWithoutUser(TestCase):
    def setUp(self):
        event = create_event(events[0])
        self.url = reverse('delete_event',kwargs={'pk':event.pk})
        self.response = self.client.get(self.url)

    def test_redirection(self):
        login_url = reverse('login')
        self.assertRedirects(self.response,f'{login_url}?next={self.url}')

class TestDeleteEventWithUserNonStaff(TestCase):
    def setUp(self):
        account_data = copy_dict(accounts[1])
        self.account = create_account(account_data)
        self.client.login(username=account_data['user']['username'],password=account_data['user']['password'])
        event = create_event(events[0])
        self.url = reverse('delete_event',kwargs={'pk':event.pk})
        self.response = self.client.get(self.url)

    def test_status_code(self):
        self.assertEqual(self.response.status_code,403)

class TestDeleteEventWithUserStaff(TestCase):
    def setUp(self):
        account_data = copy_dict(accounts[0])
        self.account = create_account(account_data)
        self.client.login(username=account_data['user']['username'],password=account_data['user']['password'])
        self.event_data = copy_dict(events[0])
        self.event = Event.objects.create(**self.event_data)
        self.url = reverse('delete_event',kwargs={'pk':self.event.pk})
        self.response = self.client.get(self.url)

    def test_status_code(self):
        self.assertEqual(self.response.status_code,200)

    def test_url_show_correct_func(self):
        view = resolve(f'/event/{self.event.pk}/delete')
        self.assertEqual(view.func.__name__,DeleteEvent.__name__)

    def test_view_show_correct_template(self):
        self.assertTemplateUsed(self.response,'event_delete.html')

    def test_form_user_csrf(self):
        self.assertContains(self.response,'csrfmiddlewaretoken')

    def test_valid_input(self):
        self.client.post(self.url)
        self.assertFalse(Event.objects.exists())

    def test_valid_input_redirection(self):
        response = self.client.post(self.url)
        redirect_url = reverse('home')
        self.assertRedirects(response,redirect_url)