from django.test import TestCase
from django.urls import reverse,resolve

from custom_code.dummy_data import events,accounts,create_account,create_event, copy_dict, to_str_event
from ..views import UpdateEvent
from ..forms import EventForm
from ..models import Event

class TestUpdateEventWithoutUser(TestCase):
    def setUp(self):
        event = create_event(events[0])
        self.url = reverse('update_event',kwargs={'pk':event.pk})
        self.response = self.client.get(self.url)

    def test_redirection(self):
        login_url = reverse('login')
        self.assertRedirects(self.response,f'{login_url}?next={self.url}')

class TestUpdateEventWithUserNonStaff(TestCase):
    def setUp(self):
        account_data = copy_dict(accounts[1])
        self.account = create_account(account_data)
        self.client.login(username=account_data['user']['username'],password=account_data['user']['password'])
        event = create_event(events[0])
        self.url = reverse('update_event',kwargs={'pk':event.pk})
        self.response = self.client.get(self.url)

    def test_status_code(self):
        self.assertEqual(self.response.status_code,403)

class TestUpdateEventWithUserStaff(TestCase):
    def setUp(self):
        account_data = copy_dict(accounts[0])
        self.account = create_account(account_data)
        self.client.login(username=account_data['user']['username'],password=account_data['user']['password'])
        self.event_data = copy_dict(events[0])
        self.event = Event.objects.create(**self.event_data)
        self.url = reverse('update_event',kwargs={'pk':self.event.pk})
        self.response = self.client.get(self.url)

    def test_status_code(self):
        self.assertEqual(self.response.status_code,200)

    def test_url_show_correct_func(self):
        view = resolve(f'/event/{self.event.pk}/update')
        self.assertEqual(view.func.__name__,UpdateEvent.__name__)

    def test_view_show_correct_template(self):
        self.assertTemplateUsed(self.response,'event_form.html')

    def test_view_show_correct_form(self):
        form = self.response.context.get('form')
        self.assertIsInstance(form,EventForm)

    def test_form_user_csrf(self):
        self.assertContains(self.response,'csrfmiddlewaretoken')

    def test_valid_input(self):
        input_data = copy_dict(self.event_data)
        input_data['shortname'] = 'abcde'
        to_str_event(input_data)
        self.client.post(self.url,input_data)
        self.event.refresh_from_db()
        self.assertEqual(self.event.shortname,input_data['shortname'])

    def test_valid_input_redirection(self):
        input_data = copy_dict(self.event_data)
        input_data['shortname'] = 'abcde'
        to_str_event(input_data)
        response = self.client.post(self.url,input_data)
        redirect_url = reverse('detail_event',kwargs={'pk':self.event.pk})
        self.assertRedirects(response,redirect_url)