from django.contrib import admin

from .models import Event,EventInvolved
# Register your models here.
admin.site.register(Event)
admin.site.register(EventInvolved)