from django.db import models

# Create your models here.
class Event(models.Model):
    name = models.CharField(max_length=150)
    shortname = models.CharField(max_length=15,blank=True,null=True)
    start = models.DateTimeField()
    end = models.DateTimeField(blank=True,null=True)
    description = models.TextField(max_length=1000, blank=True,null=True)
    location = models.CharField(max_length=150,blank=True,null=True)
    preview = models.ImageField(blank=True,null=True,upload_to='preview')

    def __str__(self):
        if self.shortname:
            return self.shortname
        else:
            return self.name

    def get_participants(self):
        """
        Get participants list for a record.
        """
        records = EventInvolved.objects.select_related('participant').filter(event=self)
        if records:
            return records
        else:
            return None


class EventInvolved(models.Model):
    event = models.ForeignKey('event',on_delete=models.CASCADE)
    participant = models.ForeignKey('account.account',on_delete=models.SET_NULL,blank=True,null=True)