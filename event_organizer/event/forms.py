from django import forms

from .models import Event

class EventForm(forms.ModelForm):
    start = forms.DateTimeField(input_formats=('%d-%m-%Y %H:%M',), 
        widget=forms.DateTimeInput(attrs={'class':'date-time-picker'}, format='%d-%m-%Y %H:%M'))
    end = forms.DateTimeField(input_formats=('%d-%m-%Y %H:%M',), required=False,
        widget=forms.DateTimeInput(attrs={'class':'date-time-picker'}, format='%d-%m-%Y %H:%M'))


    class Meta:
        model = Event
        fields = '__all__'

    def is_changed(self,field,val):
        """
        Check wether a field is changed from current record. This function used in modelform.
        Parameters
        ---------
        field: str
            Name of field to be checked
        val: any
            Value of field to be checked
        """
        if self.instance.pk:
            prev_val = getattr(self.instance,field)
            return prev_val != val
        else:
            return True

    def clean_name(self):
        """
        Validate name input. Name of an event must be unique.
        """
        data = self.cleaned_data.get('name')
        if self.is_changed('name',data):
            if Event.objects.filter(name=data).exists():
                self.add_error('name',f'Event with name {data} already exists. ')
        return data

    def clean(self):
        """
        Validate between fields. Event start must be before end.
        """
        if any(self.errors):
            return
        start = self.cleaned_data.get('start')
        end = self.cleaned_data.get('end')
        if start and end and end<start:
            self.add_error('','Start must be before end.')


class InvolvementForm(forms.Form):
    mode = forms.IntegerField(widget=forms.HiddenInput())
    event_id = forms.IntegerField(widget=forms.HiddenInput())