from django.urls import path
from . import views

urlpatterns = [
    path('',views.event,name='event'),
    path('create',views.CreateEvent.as_view(),name='create_event'),
    path('<int:pk>',views.detail_event,name='detail_event'),
    path('<int:pk>/update',views.UpdateEvent.as_view(),name='update_event'),
    path('<int:pk>/delete',views.DeleteEvent.as_view(),name='delete_event'),
]