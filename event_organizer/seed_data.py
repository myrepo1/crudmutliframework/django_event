from django.utils import timezone
from django.contrib.auth.models import User
from django.core.files import File
from datetime import timedelta
import os

from account.models import Account
from event.models import Event,EventInvolved
from event_organizer import settings

from custom_code.dummy_data import events,accounts

base_dir = settings.BASE_DIR
print('Begin create events....')
preview_dir = os.path.join(base_dir,'example_data/nature.jpeg')
preview = File(open(preview_dir,'rb'))
for item in events:
    event = Event.objects.create(**item)
    event.preview.save('event.jpg',preview)
preview.close()
print('Finish create events!!!')

print('Begin create accounts...')
profpic_dir = os.path.join(base_dir,'example_data/baby.jpeg')
profpic = File(open(profpic_dir,'rb'))
for item in accounts:
    user = User.objects.create_user(**item['user'])
    account_data = item['account']
    account_data['user'] = user
    account = Account.objects.create(**account_data)
    account.profpic.save('profpic.jpg',profpic)
profpic.close()
print('Finish create accounts!!!')

print('Assign account to event')
event = Event.objects.get(shortname='TP45')
account = Account.objects.get(phone='12930123213')
EventInvolved.objects.create(event=event,participant=account)
print('Finish assign account to event')